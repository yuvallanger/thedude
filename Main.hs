module Main (main) where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Simulate
import Graphics.Gloss.Interface.Pure.Game
import Graphics.Gloss.Interface.Pure.Display
import Graphics.Gloss.Data.Vector
import TheDude.Vector
import TheDude.Colors


main :: IO ()
main = do
  worldMapCode <- loadWorldMap worldMapFilename
  let initialWorld = parseWorldMap worldMapCode
  let ourDisplay = InWindow "The Dude"
                    (displayDimX, displayDimY)
                    (0,0)
  let ourBackgroundColor = mixColors 0.7 0.5 blue white

  let ourFPSthing = 24
  
  play
        ourDisplay
        ourBackgroundColor
        ourFPSthing
        initialWorld
        drawWorld
        handleEvents
        simulateWorld

--
-- Consts
--


displayDimX, displayDimY :: Int
displayDimX = 450
displayDimY = 450

walkVel, jumpVel :: Point
walkVel = (5, 0)
jumpVel = (0, 5)

-- Used when drawing
brickSize :: Float
brickSize = 30

--
-- Types
--

data Dude = Dude { dudePos :: Point, dudeVel :: Point }
  deriving (Eq, Ord, Show)

type Platform = (Point, Point)

data World = Play { dude :: Dude
                  , platforms :: [Platform]
                  }
           | GameOver
  deriving (Eq, Ord, Show)


-- initWorld :: worldMap -> World
-- initWorld worldMap = undefined
--   where
--   world = Play
--             Dude { dudePos = (middleOfDisplay, onTheGround) }
--             [((20, 20), (platformSizeW, platformSizeH))]


drawWorld :: World -> Picture
drawWorld GameOver
  = scale 0.3 0.3
    . translate (-400) 0
    . color red
    . text
    $ "Game Over!"
drawWorld w =
  pictures [platformsPicture, dudePicture]
  where
    (dudePosX, dudePosY) = dudePos . dude $ w
    dudePicture = color orange
              . pictures
              $ [translate dudePosX dudePosY
                 . circle $ brickSize / 2
                ]
    platformsPicture = pictures [color brown . square $ platform
                                | platform <- platforms w
                                ]

square :: (Point, Point) -> Picture
square ((posX, posY), (sizeX, sizeY)) =
  polygon [(posX        , posY        )
          ,(posX + sizeX, posY        )
          ,(posX + sizeX, posY + sizeY)
          ,(posX        , posY + sizeY)
          ,(posX        , posY        )
          ]

worldMapFilename :: String
worldMapFilename = "zeWarudo.txt"

loadWorldMap :: FilePath -> IO [String]
loadWorldMap filename= do
  fileContents <- readFile filename
  return $ lines fileContents

parseWorldMap :: [String] -> World
parseWorldMap mapCodeRaw =
  Play { dude      = Dude { dudePos = theDudePos, dudeVel = (0, 0) }
       , platforms = [(platform, (brickSize, brickSize))
                     | platform <- findBricks platformBrickKind
                     ]
   
       }
  where
  -- beginningAndEnd = [(i, drop 2 l) | (i, l) <- zip [0..] a, (take 2 l) == "[ " || (take 2 l) == "] "]
  mapCode = reverse mapCodeRaw
  mapHeight = length mapCode
  mapWidth = maximum [length row | row <- mapCode]
  dudeBrickKind = "D"
  platformBrickKind = "#?"
  findBricks :: String -> [(Float, Float)]
  findBricks kinds = [mulSV brickSize (fromInteger col, fromInteger row)
                     | (row, lineCode)  <- zip [0..] mapCode
                     , (col, brickCode) <- zip [0..] lineCode
                     , isBrickKind kinds brickCode
                     ]
  isBrickKind kinds brickCode = 0 /= length [kind | kind <- kinds, brickCode == kind]
  theDudePos = head (findBricks dudeBrickKind) .+ (brickSize / 2, brickSize / 2)

simulateWorld :: Float -> World -> World
simulateWorld _ GameOver = GameOver
simulateWorld _ w =
  let oldDude = dude w
      oldDudePos = dudePos oldDude
      oldDudeVel = dudeVel oldDude
      newDudePos = oldDudePos .+ oldDudeVel in
    w { dude = oldDude { dudePos = newDudePos } }

handleDirectionKey :: SpecialKey -> KeyState -> World -> World
handleDirectionKey a b w   =
  case (a, b) of
      (KeyLeft  , Down) -> w { dude = oldDude { dudeVel = dudeVelLeft    }}
      (KeyLeft  , Up  ) -> w { dude = oldDude { dudeVel = dudeVelNoLeft  }}
      (KeyRight , Down) -> w { dude = oldDude { dudeVel = dudeVelRight   }}
      (KeyRight , Up  ) -> w { dude = oldDude { dudeVel = dudeVelNoRight }}
      (KeyUp    , Down) -> w { dude = oldDude { dudeVel = dudeVelUp      }}
      (KeyUp    , Up  ) -> w { dude = oldDude { dudeVel = dudeVelNoUp    }}
      _                 -> w
  where
    oldDude        = dude w
    oldDudeVel     = dudeVel oldDude
    dudeVelLeft    = mulSV (-1) walkVel .+ (oldDudeVel .* (0, 1))
    dudeVelRight   = mulSV 1 walkVel .+ (oldDudeVel .* (0, 1))
    dudeVelUp      = (oldDudeVel .* (1, 0)) .+ (jumpVel .* (0, 1))
    dudeVelDown    = oldDudeVel
    dudeVelNoLeft  = mulSV 0 walkVel .+ (oldDudeVel .* (0, 1))
    dudeVelNoRight = dudeVelNoLeft
    dudeVelNoUp    = oldDudeVel
    dudeVelNoDown  = oldDudeVel

handleEvents :: Event -> World -> World
handleEvents _ GameOver = GameOver
handleEvents (EventKey (SpecialKey KeyLeft) Down _ _)
             w = handleDirectionKey KeyLeft Down w
handleEvents (EventKey (SpecialKey KeyLeft) Up _ _)
             w = handleDirectionKey KeyLeft Up w
handleEvents (EventKey (SpecialKey KeyRight) Down _ _)
             w = handleDirectionKey KeyRight Down w
handleEvents (EventKey (SpecialKey KeyRight) Up _ _)
             w = handleDirectionKey KeyRight Up w
handleEvents (EventKey (SpecialKey KeyUp) Down _ _)
             w = handleDirectionKey KeyUp Down w
handleEvents (EventKey (SpecialKey KeyUp) Up _ _)
             w = handleDirectionKey KeyUp Up w
handleEvents (EventKey (SpecialKey KeyDown) Up _ _)
             w = handleDirectionKey KeyDown Up w
handleEvents (EventKey (SpecialKey KeyDown) Down _ _)
             w = handleDirectionKey KeyDown Down w
handleEvents _ w = w
