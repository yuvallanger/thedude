module TheDude.Vector where

import Graphics.Gloss

(.-) , (.+), (.*) :: Point -> Point -> Point
(x,y) .- (u,v) = (x-u,y-v)
(x,y) .+ (u,v) = (x+u,y+v)
(x,y) .* (u,v) = (x*u,y*v)
infixl 6 .- , .+
infixl 7 .*
