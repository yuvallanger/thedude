module TheDude.Colors (brown) where

import Graphics.Gloss
import Data.Bits

brown :: Color
brown = makeColor8 0xaa 0x44 0x22 0xff

